<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
	Route::get('dashboard',['as' => 'dashboardRoute', 'uses' => 'DashboardController@index']);
	
	Route::get('categories',['as' => 'categoriesRoute', 'uses' => 'CategoryController@index']);
	Route::post('categories',['as' => 'categoriesStore', 'uses' => 'CategoryController@store']);
	Route::delete('categories/{id}',['as' => 'categoriesDestroy', 'uses' => 'CategoryController@destroy']);

	Route::resource('posts', 'PostController');
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
