@extends('admin.layout')

@section('content')

<!-- Trigger the modal with a button -->
<a  class="btn btn-info " data-toggle="modal" href="#addCategory">Add Category</a>

<table class="table table-bordered">
	<thead>
		<th>Id</th>
    <th>Name</th>
		<th>Action</th>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr>
			<td>{{ $row->id }}</td>
			<td>{{ $row->categories }}</td>
      <td>
        <button class="btn btn-warning btn-inline">Edit</i></button>
        {!! Form::open(['action' => ['CategoryController@destroy', $row->id],'method'=> 'delete', 'class'=> 'form-inline']) !!}
        <button type="submit" class="btn btn-danger btn-inline btnDelete">Delete</i></button>

        {!! Form::close() !!}
      </td>
		</tr>
		@endforeach
	</tbody>
</table>

<!-- Modal -->
<div id="addCategory" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Category</h4>
      </div>
      <div class="modal-body">

<!--       	{!! Form::open() !!}
 -->      	{{ Form::open(array('action' => 'CategoryController@store')) }}
      	    <label for="name">Name</label>
    		<input type="text" name="name" class="form-control" placeholder="Enter name">
      	
        	
        	<!-- <div class="form-group">
        		<label for="name">Name</label>
        		<input type="text" name="name" class="form-control" placeholder="Enter name">
        	</div> -->

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" >Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

      {!! Form::close() !!}
    </div>

  </div>
</div>
@endsection

@section('script')

  <script>

    $(function){
      $('.btnDelete').click(function(e){
        e.preventDefault();
        if(confirm('Are you sure to delete!')){
          return true;
        }
      });
    }

  </script>

@endsection

