@extends('admin.layout')

@section('content')

{!! Form::open(['action' => 'PostController@store', 'files' => true]) !!}

	<div class="form-group">
		{!! Form::label('Title') !!}
		{!! Form::text('title', null,['class' => 'form-control', 'required' => 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('Description') !!}
		{!! Form::textarea('description', null, ['class' => 'form-control summernote', 'required' => 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('Category') !!}
		{!! Form::select('category_id', $categoryList, 'null', ['class' => 'form-control', 'required' => 'required']  ) !!}
	</div>

	<div class="form-group">
		{!! Form::label('Feature Image') !!}
		{!! Form::file('features_image', ['class' => 'form-control', 'required' => 'required']  ) !!}
	</div>
	<div class="form-group">
		{!! Form::label('Status') !!}
		{!! Form::select('status', getPostStatusList(), 'Published', ['class' => 'form-control', 'required' => 'required']  ) !!}
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-primary">Save</button>
	</div>

{!! Form::close() !!}
@endsection