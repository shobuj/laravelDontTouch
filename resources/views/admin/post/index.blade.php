@extends('admin.layout')

@section('content')

<table class="table table-bordered">
	<thead>
		<th>Id</th>
    	<th>Title</th>
    	<th>Status</th>
		<th>Action</th>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr>
			<td>{{ $row->id }}</td>
			<td>{{ $row->title }}</td>
			<td>{{ $row->status }}</td>
      <td>
        {!! Form::open(['action' => ['PostController@destroy', $row->id],'method'=> 'delete', 'class'=> 'form-inline']) !!}
        <button type="submit" class="btn btn-danger btn-inline btnDelete">Delete</i></button>

        {!! Form::close() !!}
      </td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection