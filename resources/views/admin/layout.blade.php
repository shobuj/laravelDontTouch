<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<title>Post Create and Retieve</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('public/admin/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/admin/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/admin/dist/summernote.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/admin/css/style.css') }}">

        @yield('style')
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">My Blog</a>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="{{ url('/') }}">Home</a></li>
						</ul>

						<ul class="nav navbar-nav navbar-right">
							@if (Auth::guest())
								<li><a href="{{ url('/login') }}">Login</a></li>
								<li><a href="{{ url('/auth/register') }}">Register</a></li>
							@else
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ url('/logout') }}">Logout</a></li>
									</ul>
								</li>
							@endif
						</ul>
					</div>
				</div>
			</nav>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-2 sidbar">
				@include('admin.sidebar')
			</div>
			<div class="col-md-10">

				<div class="row">
					<div class="col-md-12">

						@if(Session::has('success'))
							<div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert">&times;</a>
								{{ Session::get('success') }}
							</div>
						@endif

						@if(Session::has('danger'))
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							{{ Session::get('danger') }}
						@endif

					</div>
				</div>

				@yield('content')

			</div>
		</div>
	</div>

	

	 	<script type="text/javascript" src="{{ asset('public/admin/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/dist/summernote.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/function.js') }}"></script>

        @yield('script')
        
</body>
</html>
