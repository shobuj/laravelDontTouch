<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Post::all();

        return view('admin.post.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryList = Category::pluck('categories', 'id');
        return view('admin.post.create', compact('categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $post->user_id     = Auth::user()->id;
        $post->post_date   = date('Y-m-d');
        $post->title=$request->input('title');
        $post->description=$request->input('description');
        $post->category_id = $request->input('category_id');
        $post->status = $request->input('status');
        //$post->save();

        // if($result){
        //                 return redirect()->back()->with('success','Post Created Successfully');

        // }

        if($post->save()){
            if($request->hasFile('features_image')){
                $photo = $request->file('features_image');
                $filename = '';
                $filename = $post->id . '.' . $photo->getClientOriginalExtension();
                $request->file('features_image')->move(getFeatureImagePath(),$filename);
                Post::find($post->id)->update(['features_image' => $filename]);

            }
            return redirect()->back()->with('success','Post Created Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
