<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class CategoryController extends Controller
{
    public function index(){

    	$data = Category::all();
    	// return view('admin.dashboard');

    	return view('admin.category.index',compact(['data']));
    }

    public function store(Request $r){

    	$category = new Category();
    	$category->categories = $r->input('name');
    	$result = $category->save();

    	if($result){
    		return redirect()->back()->with('success','Category Created successfully');
    	}
    	else{
    		return redirect()->back()->with('success','Category Not created');
    	}
    }

    public function destroy($id){
        $category = Category::find($id);

        if(count($category)){

            $category->delete();
            return redirect()->back()->with('success', 'Category Deleted successfully');

        }
    }
}
