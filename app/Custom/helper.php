<?php 
/*function withEmpty($list){
	return ['' => '']+$list;
}*/

function getPostStatusList(){
	$list = [
		"Published" => "Published",
		"Pending"   => "Pending",
		"UnPublished" => "UnPublished",
		"Draft"      => "Draft"
	];
	return $list;
}

function getCommentStatus(){
	$list = [
		"Approved" => "Approved",
		"UnApproved" => "UnApproved", 
		"Moderate"   => "Moderate"
	];
	return $list;
}

function getFeatureImageUrl($var = null){
	if($var != null){
		return asset('public/features_images'. $var . '');
	}
	else{
		return asset('public/features_images');
	}
}

function getFeatureImagePath($var = null){
	if($var != null){
		return public_path('/features_images' . $var . '');
	}
	else{
		return public_path('/features_images');
	}
}