<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;
    protected $fillable = ['post_date', 'category_id', 'user_id', 'title', 'description', 'features_image', 'status'];
}
